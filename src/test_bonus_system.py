import random
from bonus_system import calculateBonuses


def test_invalid():
    for i in [10000, 50000, 100000]:
        for j in [i - 1, i, i + 1]:
            for k in 'ABCDEFGHIKLMNOPQRSTVXYZ':
                assert calculateBonuses(k, j) == 0


def template_l(program, amount, bonuses):
    def inner():
        assert calculateBonuses(program, amount - 1) == bonuses
        assert calculateBonuses(program, amount) != bonuses
        assert calculateBonuses(program, amount + 1) != bonuses
    return inner


def template_r(program, amount, bonuses):
    def inner():
        assert calculateBonuses(program, amount - 1) != bonuses
        assert calculateBonuses(program, amount) == bonuses
        assert calculateBonuses(program, amount + 1) == bonuses
    return inner


test_standard_a = template_l('Standard', 10000, 1 * 0.5)
test_standard_b = template_r('Standard', 10000, 1.5 * 0.5)
test_standard_c = template_l('Standard', 50000, 1.5 * 0.5)
test_standard_d = template_r('Standard', 50000, 2 * 0.5)
test_standard_e = template_l('Standard', 100000, 2 * 0.5)
test_standard_f = template_r('Standard', 100000, 2.5 * 0.5)

test_premium_a = template_l('Premium', 10000, 1 * 0.1)
test_premium_b = template_r('Premium', 10000, 1.5 * 0.1)
test_premium_c = template_l('Premium', 50000, 1.5 * 0.1)
test_premium_d = template_r('Premium', 50000, 2 * 0.1)
test_premium_e = template_l('Premium', 100000, 2 * 0.1)
test_premium_f = template_r('Premium', 100000, 2.5 * 0.1)

test_diamond_a = template_l('Diamond', 10000, 1 * 0.2)
test_diamond_b = template_r('Diamond', 10000, 1.5 * 0.2)
test_diamond_c = template_l('Diamond', 50000, 1.5 * 0.2)
test_diamond_d = template_r('Diamond', 50000, 2 * 0.2)
test_diamond_e = template_l('Diamond', 100000, 2 * 0.2)
test_diamond_f = template_r('Diamond', 100000, 2.5 * 0.2)
